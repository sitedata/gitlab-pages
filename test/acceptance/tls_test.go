package acceptance_test

import (
	"crypto/tls"
	_ "embed"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

//go:embed testdata/client.crt
var fixtureClientCert string

//go:embed testdata/client.key
var fixtureClientKey string

func TestGlobalMutualTLS(t *testing.T) {
	RunPagesProcess(t,
		withListeners([]ListenSpec{httpsListener}),
		withExtraArgument("tls-client-auth", "requireandverifyclientcert"),
		withExtraArgument("tls-client-cert", "../../test/acceptance/testdata/ca.crt"),
	)

	t.Run("client with cert works", func(t *testing.T) {
		clientCert, err := tls.X509KeyPair([]byte(fixtureClientCert), []byte(fixtureClientKey))
		require.NoError(t, err)

		client, cleanup := ClientWithConfig(&tls.Config{
			Certificates: []tls.Certificate{clientCert},
		})
		defer cleanup()

		rsp, err := client.Get(httpsListener.URL("/"))
		require.NoError(t, err)
		require.NoError(t, rsp.Body.Close())
	})
	t.Run("client without cert fails", func(t *testing.T) {
		client, cleanup := ClientWithConfig(&tls.Config{})
		defer cleanup()

		rsp, err := client.Get(httpsListener.URL("/"))
		require.Error(t, err)
		// TODO: Go versions >= 1.21 return "tls: certificate required"
		require.ErrorContains(t, err, "tls: bad certificate")
		require.Nil(t, rsp)
	})
}

func TestGlobalDomainBasedMutualTLS(t *testing.T) {
	RunPagesProcess(t,
		withListeners([]ListenSpec{clientCertListener}),
		withExtraArgument("tls-client-auth", "requireandverifyclientcert"),
		withExtraArgument("tls-client-cert", "../../test/acceptance/testdata/ca.crt"),
		withExtraArgument("tls-client-auth-domains", "test.gitlab-example.com"),
	)

	noClientCertListener := ListenSpec{
		Type:       httpsListener.Type,
		Host:       httpsListener.Host,
		Port:       clientCertListener.Port,
		ClientCert: false,
	}

	t.Run("client with cert works to mtls domain", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, clientCertListener, "test.gitlab-example.com", "/")

		require.NoError(t, err)
		require.NoError(t, rsp.Body.Close())
	})
	t.Run("client without cert fails", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, noClientCertListener, "test.gitlab-example.com", "/")
		require.Error(t, err)
		// TODO: Go versions >= 1.21 return "tls: certificate required"
		require.ErrorContains(t, err, "tls: bad certificate")
		require.Nil(t, rsp)
	})
	t.Run("client without cert works to non-mtls domain", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, noClientCertListener, "other-test.gitlab-example.com", "/")

		require.NoError(t, err)
		require.NoError(t, rsp.Body.Close())
	})
}

func TestGitLabAPIBasedMutualTLS(t *testing.T) {
	RunPagesProcess(t,
		withListeners([]ListenSpec{clientCertListener}),
	)

	noClientCertListener := ListenSpec{
		Type:       httpsListener.Type,
		Host:       httpsListener.Host,
		Port:       clientCertListener.Port,
		ClientCert: false,
	}

	t.Run("client with cert works to mtls domain", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, clientCertListener, "mtls.gitlab-example.com", "/")

		require.NoError(t, err)
		require.EqualValues(t, http.StatusOK, rsp.StatusCode)
		require.NoError(t, rsp.Body.Close())
	})
	t.Run("client without cert fails", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, noClientCertListener, "mtls.gitlab-example.com", "/")
		require.Error(t, err)
		// newer Go versions return "tls: certificate required"
		require.ErrorContains(t, err, "tls: bad certificate")
		require.Nil(t, rsp)
	})
	t.Run("client without cert works to non-mtls domain", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, noClientCertListener, "group.gitlab-example.com", "/")

		require.NoError(t, err)
		require.NoError(t, rsp.Body.Close())
		require.EqualValues(t, http.StatusOK, rsp.StatusCode)
	})
	t.Run("client with cert works to non-mtls domain", func(t *testing.T) {
		rsp, err := GetPageFromListener(t, clientCertListener, "group.gitlab-example.com", "/")

		require.NoError(t, err)
		require.NoError(t, rsp.Body.Close())
		require.EqualValues(t, http.StatusOK, rsp.StatusCode)
	})
}

func TestAcceptsSupportedCiphers(t *testing.T) {
	RunPagesProcess(t,
		withListeners([]ListenSpec{httpsListener}),
	)

	tlsConfig := &tls.Config{
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
		},
	}
	client, cleanup := ClientWithConfig(tlsConfig)
	defer cleanup()

	rsp, err := client.Get(httpsListener.URL("/"))
	require.NoError(t, err)

	t.Cleanup(func() {
		rsp.Body.Close()
	})
}

func tlsConfigWithInsecureCiphersOnly() *tls.Config {
	return &tls.Config{
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA,
			tls.TLS_RSA_WITH_3DES_EDE_CBC_SHA,
		},
		MaxVersion: tls.VersionTLS12, // ciphers for TLS1.3 are not configurable and will work if enabled
	}
}

func TestRejectsUnsupportedCiphers(t *testing.T) {
	RunPagesProcess(t,
		withListeners([]ListenSpec{httpsListener}),
	)

	client, cleanup := ClientWithConfig(tlsConfigWithInsecureCiphersOnly())
	defer cleanup()

	rsp, err := client.Get(httpsListener.URL("/"))
	require.Nil(t, rsp)
	require.Error(t, err)
}

func TestEnableInsecureCiphers(t *testing.T) {
	RunPagesProcess(t,
		withListeners([]ListenSpec{httpsListener}),
		withExtraArgument("-insecure-ciphers", "true"),
	)

	client, cleanup := ClientWithConfig(tlsConfigWithInsecureCiphersOnly())
	defer cleanup()

	rsp, err := client.Get(httpsListener.URL("/"))
	require.NoError(t, err)
	t.Cleanup(func() {
		rsp.Body.Close()
	})
}

func TestTLSVersions(t *testing.T) {
	tests := map[string]struct {
		tlsMin      string
		tlsMax      string
		tlsClient   uint16
		expectError bool
	}{
		"client version not supported":             {tlsMin: "tls1.2", tlsMax: "tls1.3", tlsClient: tls.VersionTLS10, expectError: true},
		"client version supported":                 {tlsMin: "tls1.2", tlsMax: "tls1.3", tlsClient: tls.VersionTLS12, expectError: false},
		"client and server using default settings": {tlsMin: "", tlsMax: "", tlsClient: 0, expectError: false},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			var args []string
			if tc.tlsMin != "" {
				args = append(args, "-tls-min-version", tc.tlsMin)
			}
			if tc.tlsMax != "" {
				args = append(args, "-tls-max-version", tc.tlsMax)
			}

			RunPagesProcess(t,
				withListeners([]ListenSpec{httpsListener}),
				withArguments(args),
			)

			tlsConfig := &tls.Config{}
			if tc.tlsClient != 0 {
				tlsConfig.MinVersion = tc.tlsClient
				tlsConfig.MaxVersion = tc.tlsClient
			}
			client, cleanup := ClientWithConfig(tlsConfig)
			defer cleanup()

			rsp, err := client.Get(httpsListener.URL("/"))

			if tc.expectError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				rsp.Body.Close()
			}
		})
	}
}
